# PyTest and UnitTest Notes
Referenced from [pytest site] (https://pytest.org/)

## Terminology
	* unittest - tests one piece of functionality and must be independent (python builtin)
	* TestCase - a class that runs each containing unittest
	* xfail - declares test as missing features
	* marks - apply meta data
	* fixtures - declared argument names requested by test functions
	* item - test invocation

## Helpful Command Line Options
	* v - verbose, prints each unittest result
	* s - sysout, prints all console output
	* k - calls all instances of item

## Basic Usage
	* for test scripts to be recognized the file should start with 'test'
	* all uniit tests must start with 'test'
	* all test cases must start with 'Test'
	* unittest fixtures
		* setup
		'''
			class Tester1(unittest.TestCase):
				@pytest.fixture(autouse=True)
				def doThisEveryTimeBefore(self):
					print 'setup'
		'''
		or
		'''
			class Tester1(unittest.TestCase):
				def setup_method(self, method):
					print 'setup'
		'''
		* teardown
		'''
			class Tester1(unittest.TestCase):
				@pytest.fixture(autouse=True)
				def doThisEveryTimeAfter(self):
					print 'teardown'
		'''
		or
		'''
			class Tester1(unittest.TestCase):
				def teardown_method(self, method):
					print 'teardown'
		'''
	* perform TestCase initialization using
		'''
			class Tester1(unittest.TestCase):
				def setup_class(self):
					print 'setup'
		'''
		or
		'''
			class Tester1(unittest.TestCase):
				def teardown_class(self):
					print 'teardown'
		'''
